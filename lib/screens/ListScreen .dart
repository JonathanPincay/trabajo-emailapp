import 'package:flutter/material.dart';
import 'package:flutter_application_mail/model/email.dart';
import 'package:flutter_application_mail/model/backend.dart';
import 'package:flutter_application_mail/screens/DetailScreen.dart';

class ListScreen extends StatefulWidget {
  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  List<Email> emails = Backend().getEmails();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Mock mail'),
          centerTitle: true,
        ),
        body: ListView.builder(
            itemCount: emails.length,
            itemBuilder: (BuildContext context, int index) {
              final email = emails[index];
              final id = email.id;
              final bool read = email.read;
              return Dismissible(
                key: ValueKey(id),
                direction: DismissDirection.endToStart,
                background: Container(
                  color: Colors.green,
                  child: Icon(
                    Icons.delete,
                    color: Colors.white,
                    size: 45,
                  ),
                ),
                child: Container(
                  child: Column(
                    children: [
                      ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              email.from,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 13),
                            ),
                            
                          ],                         
                        ),                       
                        subtitle: Container(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(email.subject,
                                    style: TextStyle(
                                      fontSize: 15,
                                    )),
                                 Text(
                                  email.dateTime.toString(),
                                  style: TextStyle(color: Colors.green, fontSize: 10),
                                ),
                                 Icon(
                                    read
                                        ? Icons.messenger_outline_rounded
                                        : Icons.messenger_rounded,
                                    color: Colors.green,
                                ),
                              ]),
                        ),
                        onLongPress: () {
                          Backend().markEmailAsRead(id);
                          setState(() {});
                        },
                        onTap: () {
                          Backend().markEmailAsRead(id);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DetailScreen(
                                    email: emails[index],
                                  )));
                          setState(() {});
                        },
                      ),
                      Divider(color: Colors.green),
                    ],
                  ),
                ),
                onDismissed: (direction) {
                  setState(() {
                    emails.removeAt(index);
                    Backend().deleteEmail(id);
                    print(toString());
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        duration: Duration(seconds: 1),
                        content: Text( "$id")));
                    setState(() {});
                  });
                },
              );
            }));
  }
}
